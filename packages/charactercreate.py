import os
import sys
import time
import sqlite3
from nevertext import player

###################################################################################
#                   Define Game variables
###################################################################################
#this is going to require a LOT of rework

#commenting out all code so i can rework this correctly. basic implimentation for learning relative imports done
#don't use any variables and functions i can use in nevertext.py
#this should just be used to define the variables that i will apply to the player class

"""
i don't know if it's worth it to break the whole character creation out... this will be saved for later
"""


def start():
    name = input("Please select a Name for yourself:\n> ")
    if name == '':
        print("please select a name")
    else:
        print(f"Welcome {name}")
'''
playerig = None

def start():
    os.system(tclear)
    global playerig
    playerig = player("")
    option = input("Select a name for yourself\n>")
    playerig = player(option)
    #playerig.name = option
    namecheck()

def namecheck():
    #UBUNTU
    #conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
    #OS X
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    try:
        with conn:
            conn.execute(f"""INSERT INTO PLAYER (Name, level, pclass, race, maxhealth, health, maxmana, mana, potions, exp, levelupxp, str, sta, agi, dex, cha, int, wis, gold, resist, quest, inventory, chest, arms, wrist, hands, legs, feet, head, neck, finger1, finger2, finger3, finger4, earleft, earright, primaryw, secondary, range, zone_id, melee, spells)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """, (playerig.name, playerig.level, playerig.pclass, playerig.race, playerig.maxhealth, playerig.health, playerig.maxmana, playerig.mana, playerig.potions, playerig.exp, playerig.levelupxp, playerig.str, playerig.sta, playerig.agi, playerig.dex, playerig.cha, playerig.int, playerig.wis, playerig.gold, playerig.resist, playerig.quest, playerig.inventory, playerig.chest, playerig.arms, playerig.wrist, playerig.hands, playerig.legs, playerig.feet, playerig.head, playerig.neck, playerig.finger1, playerig.finger2, playerig.finger3, playerig.finger4, playerig.earleft, playerig.earright, playerig.primaryw, playerig.secondary, playerig.range, playerig.zone_id, playerig.melee, playerig.spells))
            
            conn.commit()
    except sqlite3.IntegrityError:
        print("Record Already Exists")
        input("Press Enter to try a new name")
        start()
    finally:
        print(f"Welcome {playerig.name}!")
        input("Press Enter to select a race")

    conn.commit()
    conn.close()
    race()

def loadcheck():
    pass
#in the database, set each race to a specific ID. then set the player.race to INT() and then build a query to call the array and the classes available to it. 
# lots of database mapping needed to shorted this out.
#this needs to be changed to call the class table when i remake that to use scripts to assign the variable values.
def race():
    os.system(tclear)
    print("Please select a race")
    print("1. Human\n2. Wood Elf\n3. Ogre\n4. Dwarf\n5. High Elf\n6. Troll\n7. Dark Elf")
    option = input("> ")
    if option == "1":
        os.system(tclear)
        print("You have chosen Human. The strength of the Human race lies in its diversity of thought, belief, and profession; though they tend be weaker than many other races.  They have no particular specialty with the sword or arcane magic.  While that is true, Humans are cunning and possess great ingenuity, which gives them an advantage.  The Human mind is sharp enough to adapt to nearly all forms of study and this gives them a great range of options when choosing a profession.")
        print("Humans often divide themselves with their own squabbles, but they will always return to their brother's side in times of crisis, or to protect a mutual interest.")
        print("Available classes: Warrior, Shadow Knight, Necromancer, Rogue, Wizard, Cleric, Druid, Bard, Monk, Enchanter, Magician, Paladin, Ranger")
        option = input("Are you sure you want to be a Human?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 1
            playerig.str += 3
            playerig.sta += 3
            playerig.dex += 4
            playerig.agi += 5
            playerig.cha += 5
            playerig.int += 4
            playerig.wis += 3
            playerig.resist += 3
            playerig.diety = "Mithaniel Marr, god of Valor, and Erollisi Marr, goddess of Love"
            playerig.dietylore = "As decendants of the original creation, the Barbarrians, you follow the Marr twins. Mithaniel Marr, ruler of the Plane of Valor, is the ultimate Paladin. You must follow his leadership and protect Norrath from a terrible tragedy."
            playerig.ac += 5
            classadd()
        else:
            race()
    elif option == "2":
        os.system(tclear)
        print("You have chosen Wood Elf. The Wood Elves, or Feir`Dal, are more accepting of others than their cousins, the High Elves.  Like their Elven cousins, Wood Elves only reach about five feet in height and have very attractive and subtle features.  They skins are tinged an oaken colour so that they blend with their natural surroundings in the forests.")
        print("Wood Elves generally regard those who respect nature as friends, especially rangers and druids.  The Wood Elves believe in co-existing with their natural surroundings as opposed to shaping them.  This leads most of their kind to worship Tunare, the Mother of All.  The Feir`Dal harness the powers of song and the living, breathing properties of their environment.  Wood Elves are known for their love of celebration and good food.  A good song will also tweak their pointed ears.")
        print("Wood Elves have access to the following classes:")
        print("Warrior, Druid, Cleric, Bard, Rogue, Ranger")
        option = input("Are you sure you want to be a Wood Elf?\n1. Yes\n2. No\n> ")
        if option =="1":
            playerig.race = 3
            playerig.str += 2
            playerig.sta += 2
            playerig.dex += 5
            playerig.agi += 7
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 3
            playerig.resist += 3
            playerig.diety = "Tunare, The Mother of All."
            playerig.dietylore = "Tunare created the good elves of Norrath and is very protective of them. As one of her creations, you must follow her wishes and help protect her children."
            playerig.ac += 3
            classadd()
        else:
            race()
    elif option == "3":
        os.system(tclear)
        print("You have chosen Ogre, With their lumbering gait and enormous breadth, Ogres are an ominous presence.  Created by Rallos Zek, these massive creatures are feared in battle for their raw strength and uncanny endurance.  While not very intelligent, Ogres are known to cooperate with one another quite efficiently.")
        print("The Ogres have a twisted sense of duty that could easily be mistaken for stupidity.  They are, justifiably, quite single-minded.  Their thick skulls also give them a  high level of resistance to being stunned in battle.")
        print("The following classes are available to you:")
        print("Warior, Shadow Knight, Shaman")
        option = input("Are you sure you want to be an Ogre?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 2
            playerig.str += 8
            playerig.sta += 7
            playerig.dex += 3
            playerig.agi += 2
            playerig.cha += 1
            playerig.int += 1
            playerig.wis += 3
            playerig.resist += 5
            playerig.diety = "Rallos Zek. The Warlord"
            playerig.dietylore = "Rallos Zek believes in survival of the strong and death to the weak. I come to you to implore you, follow your master's desire and prepare for war."
            playerig.ac += 8
            classadd()
        else:
            race()
    elif option == "4":
        os.system(tclear)
        print("You have chosen Dwarf. Though made lower to the ground than most races, the Dwarves created by Brell Serillis burst with pride and determination.  The stout and sturdy Dwarves are extremely strong, making them one of the best-suited to endure long and intense battles.  Dwarves have broad faces, usually with a prominent nose, and take extraordinary pride in their hair.  Males often take great care to grow long beards and moustaches and females often tie their hair up in a bun.  Some females also sport a finely trimmed beard.")
        print("This ancient race is strongly motivated by honour and loyalty.  They also know how to balance work and play, and know how to tell a good story.  If a Dwarf is ever wronged, it can be painful for the wrongdoer as Dwarves tend to hold a grudge.")
        print("Dwarves can be somewhat abrasive, but find friendship easiest with the Barbarians, Humans, Gnomes, and Halflings.  The Dwarves tolerate the Elves of light, but they despise the Dark Elves, Trolls, Ogres, and other evil races.")
        print("The following classes are available to you:")
        print("Warrior, Paladin, rogue, Cleric")
        option = input("Do you wish to be a Dwarf?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 4
            playerig.str += 5
            playerig.sta += 5
            playerig.dex += 5
            playerig.agi += 4
            playerig.cha += 2
            playerig.int += 3
            playerig.wis += 4
            playerig.resist += 2
            playerig.diety = " Brell Serilis, The Duke of Below "
            playerig.dietylore = "Brell Serilis, and his creations, find sanctuary in the caves, caverns and tunnels that permeate the belly of Norrath. Protecting the Underfoot of Norrath is of great importance, as much as the world above. You must use the strength given to you by your master to save Norrath."
            playerig.ac += 7
            classadd()
        else:
            race()
    elif option == "5":
        os.system(tclear)
        print("You have chosen High Elf. The High Elves, also known as the Koada`Dal, are regarded as the closest relatives of the original Norrathian Elves created by Tunare, Mother of All. ")
        print("These noble creatures stand tall with ivory skin and gentle features.  They are certainly not the most sturdy of races, but make up for that with heart.  Throughout the ages, they have stood for freedom and justice, fighting evil with powerful magic and their skills with the blade.")
        print("The High Elves are known for their civilized conduct and, as such, are perceived as a race rather full of their own importance, though they are quite benevolent.  They expect to be treated with respect and do have an haughty pride about them.  As a result, it is difficult to make friends with a High Elf.")
        print("High Elves can excel in the following classes:")
        print("Warrior, Paladin, Cleric, Wizard, Magician, Enchanter")
        option = input("Do you wish to be a High Elf?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 5
            playerig.str += 3
            playerig.sta += 3
            playerig.dex += 3
            playerig.agi += 3
            playerig.cha += 2
            playerig.int += 6
            playerig.wis += 7
            playerig.resist += 3
            playerig.diety = "Tunare, The Mother of All."
            playerig.dietylore = "Tunare created the good elves of Norrath and is very protective of them. As one of her creations, you must follow her wishes and help protect her children."
            playerig.ac += 4
            classadd()
        else:
            race()
    elif option == "6":
        os.system(tclear)
        print("You have chosen Troll. Trolls are often considered a hideous and deplorable race that carries a sickening smell.  This view suits the Troll as they are generally unpleasant to everyone.  Trolls are an immense race, at least twice as strong as the average human.  They have the ability to regenerate faster than most other races.  Trolls have two primary motivators: food and power.  These grotesque creatures will eat just about anything.")
        print("Trolls are extremely cruel and their greed is almost insatiable.  They trust no one, not even each other.  Trolls also fancy they have advanced abilities in the culinary arts, with their ability to pickle just about anything, or make a meal out of 'spare parts.'")
        print("Trolls can be the following:")
        print("Warrior, Shadow Knight, Shaman")
        option = input("Do you wish to be a Troll?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 6
            playerig.str += 6
            playerig.sta += 7
            playerig.dex += 4
            playerig.agi += 3
            playerig.cha += 0
            playerig.int += 2
            playerig.wis += 5
            playerig.resist += 3
            playerig.diety = " Cazic-Thule, The Faceless, Lord of Fear "
            playerig.dietylore = "Cazic-Thule believes in creating fear in all the creatures of Norrath. You must cast aside your masters given fear and rise up to save Norrath."
            playerig.ac += 7
            classadd()
        else:
            race()
    elif option == "7":
        os.system(tclear)
        print("You have selected Dark Elf. The Teir'Dal, or Dark Elves, are a twisted reflection of the Elves of light, but are just as lithe and intelligent as their cousins.  Their features are often soft and delicate, though the eyes of the Dark Elves betray their darker inclinations.  The Dark Elves' skin ranges from blue tones to black and their hair is most often white with some variances.  They reach a height of about 5 feet tall.")
        print(" Innoruuk, Lord of Hate, created the Teir`Dal and his hatred flows through their veins and moves their black hearts.  It runs so deep that the Teir`Dal hate all, including their own kind and their creator. The Dark Elves view all other races as inferior; tools to be used for their own evil manipulations.  Most races fear the Dark Elves.  ")
        print("Dark Elves can excel in the following classes:")
        print("Warrior, Shadow Knight, Cleric, Wizard, Magician, Enchanter, Necromancer")
        option = input("Do you wish to be a Dark Elf?\n1. Yes\n2. No\n> ")
        if option == "1":
            playerig.race = 7
            playerig.str += 2
            playerig.sta += 2
            playerig.dex += 3
            playerig.agi += 3
            playerig.cha += 1
            playerig.int += 7
            playerig.wis += 7
            playerig.resist += 5
            playerig.diety = " Innoruuk, The Prince of Hate "
            playerig.dietylore = "Though you may be sparated from your High Elven ancestry, corrupted by Innoruuk through royal High Elven blood, you still must protect Norrath and all those who live upon it in these dark times."
            playerig.ac += 4
            classadd()
        else:
            race()
    else:
        race()

        


#this needs to be changed to call the class table when i remake that to use scripts to assign the variable values.
def classadd():
    os.system(tclear)
    print("Now that you have selected your race, it's time to choose a profession.")
    print("This option will set the tone for your playstyle, think hard about it")
    print("Are you more of a 'dive straight into battle' kind of person? A warrior may be for you")
    print("Do you prefer to stay back and let others fight while you maintain their health? A cleric would be a good choice")
    print("Do you want to blast enemies with incredible spells? A wizard might be a good choice")
    #pclassa = ['Warrior', 'Shadow Knight', 'Paladin', 'Rogue', 'Cleric', 'Wizard', 'Magician', 'Enchanter', 'Monk', 'Druid', 'Necromancer', 'Ranger', 'Bard']
    if playerig.race == 1:
        print("As a Human, you have access to the majority of classes, more than any other race")
        print("Your adaptive mind and body allows you to select the following classes:")
        option = input("1. Warrior\n2. Shadow Knight\n3. Paladin\n4. Rogue\n5. Cleric\n6. Wizard\n7. Magician\n8. Enchanter\n9. Monk\n10. Druid\n11. Necromancer\n12. Ranger\n13. Bard\n> " )
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 2
            playerig.str += 9
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 0
            playerig.int += 4
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 3
            playerig.str += 8
            playerig.sta += 6
            playerig.dex += 5
            playerig.agi += 4
            playerig.cha += 4
            playerig.int += 0
            playerig.wis += 3
            playerig.resist += 8
            pclass()
        elif option == "4":
            playerig.pclass = 8
            playerig.str += 3
            playerig.sta += 2
            playerig.dex += 9
            playerig.agi += 9
            playerig.cha += 3
            playerig.int += 2
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        elif option == "5":
            playerig.pclass = 5
            playerig.str += 2
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 3
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 10
            playerig.resist += 8
            pclass()
        elif option == "6":
            playerig.pclass = 14
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 4
            playerig.int += 12
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option == "7":
            playerig.pclass = 10
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 5
            playerig.int += 11
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option == "8":
            playerig.pclass = 12
            playerig.str += 1
            playerig.sta += 3
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 8
            playerig.int += 12
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        elif option == "9":
            playerig.pclass = 11
            playerig.str += 5
            playerig.sta += 5
            playerig.dex += 10
            playerig.agi += 8
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "10":
            playerig.pclass = 4
            playerig.str += 2
            playerig.sta += 4
            playerig.dex += 4
            playerig.agi += 3
            playerig.cha += 4
            playerig.int += 5
            playerig.wis += 8
            playerig.resist += 8
            pclass()
        elif option == "11":
            playerig.pclass = 7
            playerig.str += 1
            playerig.sta += 3
            playerig.dex += 1
            playerig.agi += 1
            playerig.cha += 5
            playerig.int += 15
            playerig.wis += 4
            playerig.resist += 10
            pclass()
        elif option == "12":
            playerig.pclass = 9
            playerig.str += 3
            playerig.sta += 4
            playerig.dex += 10
            playerig.agi += 10
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 3
            playerig.resist += 8
            pclass()
        elif option == "13":
            playerig.pclass = 13
            playerig.str += 4
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 1
            playerig.resist += 10
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
    elif playerig.race == 3:
        #Warrior, Druid, Cleric, Bard, Rogue, Ranger
        print("A Wood Elf has access to the following professions:")
        option = input("1. Warrior\n2. Druid\n3. Cleric\n4. Bard\n5. Rogue\n6. Ranger\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 4
            playerig.str += 2
            playerig.sta += 4
            playerig.dex += 4
            playerig.agi += 3
            playerig.cha += 4
            playerig.int += 5
            playerig.wis += 8
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 5
            playerig.str += 2
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 3
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 10
            playerig.resist += 8
            pclass()
        elif option == "4":
            playerig.pclass = 13
            playerig.str += 4
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 1
            playerig.resist += 10
            pclass()
        elif option == "5":
            playerig.pclass = 8
            playerig.str += 3
            playerig.sta += 2
            playerig.dex += 9
            playerig.agi += 9
            playerig.cha += 3
            playerig.int += 2
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        elif option == "6":
            playerig.pclass = 9
            playerig.str += 3
            playerig.sta += 4
            playerig.dex += 10
            playerig.agi += 10
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 3
            playerig.resist += 8
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
    elif playerig.race == 2:
        #Warior, Shadow Knight, Shaman
        print("Being an Ogre, you have more limited choices in classes")
        print("However, your defensive and offensive stats will be considerable higher")
        option = input("1. Warrior\n2. Shadow Knight\n3. Shaman\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 2
            playerig.str += 9
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 0
            playerig.int += 4
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 6
            playerig.str += 3
            playerig.sta += 4
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 3
            playerig.int += 0
            playerig.wis += 10
            playerig.resist += 10
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
    elif playerig.race == 4:
        print("Dwarves have access to more 'noble' professions")
        option = input("1. Warrior\n2. Paladin\n3. Rogue\n4. Cleric\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 3
            playerig.str += 8
            playerig.sta += 6
            playerig.dex += 5
            playerig.agi += 4
            playerig.cha += 4
            playerig.int += 0
            playerig.wis += 3
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 8
            playerig.str += 3
            playerig.sta += 2
            playerig.dex += 9
            playerig.agi += 9
            playerig.cha += 3
            playerig.int += 2
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        elif option == "4":
            playerig.pclass = 5
            playerig.str += 2
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 3
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 10
            playerig.resist += 8
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
        #Warrior, Paladin, rogue, Cleric
    elif playerig.race == 5:
        print("High Elves have powerful magic abilities, making them more effective as magic wielders")
        option = input("1. Warrior\n2. Paladin\n3. Cleric\n4. Wizard\n5. Magician\n6. Enchanter\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 3
            playerig.str += 8
            playerig.sta += 6
            playerig.dex += 5
            playerig.agi += 4
            playerig.cha += 4
            playerig.int += 0
            playerig.wis += 3
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 5
            playerig.str += 2
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 3
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 10
            playerig.resist += 8
            pclass()
        elif option == "4":
            playerig.pclass = 14
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 4
            playerig.int += 12
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option =="5":
            playerig.pclass = 10
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 5
            playerig.int += 11
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option == "6":
            playerig.pclass = 12
            playerig.str += 1
            playerig.sta += 3
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 8
            playerig.int += 12
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
        #Warrior, Paladin, Cleric, Wizard, Magician, Enchanter
    elif playerig.race == 6:
        print("Trolls have access to the same classes as Ogres.")
        print("Trolls have a higher hp regen at the cost of some strength and stamina")
        option = input("1. Warrior\n2. Shadow Knight\n3. Shaman\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 2
            playerig.str += 9
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 0
            playerig.int += 4
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 6
            playerig.str += 3
            playerig.sta += 4
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 3
            playerig.int += 0
            playerig.wis += 10
            playerig.resist += 10
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
        #Warrior, Shadow Knight, Shaman
    elif playerig.race == 7:
        print("Dark Elves, like their high born cousins, have high magical properties")
        option = input("1. Warrior\n2. Shadow Knight\n3. Cleric\n4. Wizard\n5. Magician\n6. Enchanter\n7. Necromancer\n> ")
        if option == "1":
            playerig.pclass = 1
            playerig.str += 10
            playerig.sta += 8
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 2
            playerig.int += 0
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "2":
            playerig.pclass = 2
            playerig.str += 9
            playerig.sta += 7
            playerig.dex += 5
            playerig.agi += 5
            playerig.cha += 0
            playerig.int += 4
            playerig.wis += 0
            playerig.resist += 8
            pclass()
        elif option == "3":
            playerig.pclass = 5
            playerig.str += 2
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 3
            playerig.cha += 5
            playerig.int += 3
            playerig.wis += 10
            playerig.resist += 8
            pclass()
        elif option == "4":
            playerig.pclass = 14
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 4
            playerig.int += 12
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option == "5":
            playerig.pclass = 10
            playerig.str += 1
            playerig.sta += 5
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 5
            playerig.int += 11
            playerig.wis += 4
            playerig.resist += 8
            pclass()
        elif option == "6":
            playerig.pclass = 12
            playerig.str += 1
            playerig.sta += 3
            playerig.dex += 2
            playerig.agi += 2
            playerig.cha += 8
            playerig.int += 12
            playerig.wis += 2
            playerig.resist += 8
            pclass()
        elif option == "7":
            playerig.pclass = 7
            playerig.str += 1
            playerig.sta += 3
            playerig.dex += 1
            playerig.agi += 1
            playerig.cha += 5
            playerig.int += 15
            playerig.wis += 4
            playerig.resist += 10
            pclass()
        else:
            print("That is not a valid option")
            input("Press Enter to try again")
            classadd()
        #Warrior, Shadow Knight, Cleric, Wizard, Magician, Enchanter, Necromancer




#add stat changes
def pclass():
    #UBUNTU
    #conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
    #OS X
    playerig.primaryw = 1
    playerig.health = playerig.sta * 2
    playerig.maxhealth = playerig.sta * 2
    playerig.levelupxp = 300
    playerig.meleeskills = 1
    playerig.spellskills = 1
    playerig.melee = 1 #c.execute(f"SELECT SPELL.Name FROM SPELL JOIN PLAYER ON SPELL.pclass = PLAYER.pclass WHERE PLAYER.Name = '{playerig.name}'")
    playerig.spells = 1 #c.execute(f"SELECT Name FROM SPELL WHERE pclass = {playerig.pclass}")
    playerig.meleeskillup = 20
    playerig.spellskillup = 20
    playerig.MSXP = 0
    playerig.SSXP = 0
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    cursorObj = conn.cursor()
    cursorObj.execute(f"""UPDATE PLAYER 
        SET level = {playerig.level},
        pclass = {playerig.pclass},
        race = {playerig.race},
        maxhealth = {playerig.maxhealth},
        health = {playerig.health},
        maxmana = {playerig.maxmana},
        mana = {playerig.mana},
        potions = {playerig.potions},
        exp = {playerig.exp},
        levelupxp = {playerig.levelupxp},
        str = {playerig.str},
        sta = {playerig.sta},
        agi = {playerig.agi},
        dex = {playerig.dex},
        cha = {playerig.cha},
        int = {playerig.int},
        wis = {playerig.wis},
        gold = {playerig.gold},
        resist = {playerig.resist},
        quest = "{playerig.quest}",
        inventory = "{playerig.inventory}",
        chest = {playerig.chest},
        arms = {playerig.arms},
        wrist = {playerig.wrist},
        hands = {playerig.hands},
        legs = {playerig.legs},
        feet = {playerig.feet},
        head = {playerig.head},
        neck = {playerig.neck},
        finger1 = {playerig.finger1},
        finger2 = {playerig.finger2},
        finger3 = {playerig.finger3},
        finger4 = {playerig.finger4},
        earleft = {playerig.earleft},
        earright = {playerig.earright},
        primaryw = {playerig.primaryw},
        secondary = {playerig.secondary},
        range = {playerig.range},
        zone_id = {playerig.zone_id},
        melee = {playerig.melee},
        spells = {playerig.spells},
        diety = "{playerig.diety}",
        dietylore = "{playerig.dietylore}",
        ac = {playerig.ac},
        meleeskills = {playerig.meleeskills},
        spellskills = {playerig.spellskills},
        meleeskillup = {playerig.meleeskillup},
        spellskillup = {playerig.spellskillup},
        MSXP = {playerig.MSXP},
        SSXP = {playerig.SSXP}
        WHERE Name like '{playerig.name}';""")

    conn.commit()
    conn.close()
    print("You have successfully created your character")
    input("Press Enter to continue")
    intro()
'''