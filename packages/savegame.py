###################################################################################
#                   IMPORTS - SaveGame
###################################################################################

from __future__ import division
from __future__ import absolute_import
import os
import sys
import sqlite3 
from os import system
import random
import json
import time
#from packages import charactercreate
#from packages import tutorial




###################################################################################
#                   Database Code and connections
###################################################################################
#init connection (always active, or should i call this?)
#this needs to be in the OS check function to allow me to define where the database is saved...
#there has to be a better way.
#UBUNTU
#conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
#OS X
conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
c = conn.cursor()
#def save should not have the save prompt. that should be a pre-save function so i can dynamically call
#the save function and have it save without the player knowing or just pass
#a comment saying "game saved" or something like that
def save():
    os.system('clear')
    cursorObj = conn.cursor()
    print("would you like to save your game?")
    print("1. Yes\2. No")
    option = input("> ")
    if option == "1":
        cursorObj.execute(f"""UPDATE PLAYER 
        SET level = {playerig.level},
        pclass = {playerig.pclass},
        race = {playerig.race},
        maxhealth = {playerig.maxhealth},
        health = {playerig.health},
        maxmana = {playerig.maxmana},
        mana = {playerig.mana},
        potions = {playerig.potions},
        exp = {playerig.exp},
        levelupxp = {playerig.levelupxp},
        str = {playerig.str},
        sta = {playerig.sta},
        agi = {playerig.agi},
        dex = {playerig.dex},
        cha = {playerig.cha},
        int = {playerig.int},
        wis = {playerig.wis},
        gold = {playerig.gold},
        resist = {playerig.resist},
        quest = "{playerig.quest}",
        inventory = "{playerig.inventory}",
        chest = {playerig.chest},
        arms = {playerig.arms},
        wrist = {playerig.wrist},
        hands = {playerig.hands},
        legs = {playerig.legs},
        feet = {playerig.feet},
        head = {playerig.head},
        neck = {playerig.neck},
        finger1 = {playerig.finger1},
        finger2 = {playerig.finger2},
        finger3 = {playerig.finger3},
        finger4 = {playerig.finger4},
        earleft = {playerig.earleft},
        earright = {playerig.earright},
        primaryw = {playerig.primaryw},
        secondary = {playerig.secondary},
        range = {playerig.range},
        zone_id = {playerig.zone_id},
        melee = {playerig.melee},
        spells = {playerig.spells},
        diety = "{playerig.diety}",
        dietylore = "{playerig.dietylore}",
        ac = {playerig.ac},
        meleeskills = {playerig.meleeskills},
        spellskills = {playerig.spellskills},
        meleeskillup = {playerig.meleeskillup},
        spellskillup = {playerig.spellskillup},
        MSXP = {playerig.MSXP},
        SSXP = {playerig.SSXP}
        WHERE Name like '{playerig.name}';""")
        
        

        conn.commit()
        #conn.close()
        print("Game saved.")
        input("Press ENTER to continue...")
        
    else:
        pass
#quicksave is used to quickly call the save function without the prompts. 
#used for class/race designation and quest completions.
def quicksave():
    conn.executescript(
            f"""UPDATE PLAYER SET 
            level = {playerig.level},
            pclass = '{playerig.pclass}',
            race = '{playerig.race}',
            maxhealth = {playerig.maxhealth},
            health = {playerig.health},
            maxmana = {playerig.maxmana},
            mana = {playerig.mana},
            potions = {playerig.potions},
            exp = {playerig.exp},
            levelupxp = {playerig.levelupxp},
            str = {playerig.str},
            sta = {playerig.sta},
            agi = {playerig.agi},
            dex = {playerig.dex},
            cha = {playerig.cha},
            int = {playerig.int},
            wis = {playerig.wis},
            gold = {playerig.gold},
            resist = {playerig.resist},
            quest = {playerig.quest},
            inventory = {playerig.inventory},
            chest = {playerig.chest},
            arms = {playerig.arms},
            wrist = {playerig.wrist},
            hands = {playerig.hands},
            legs = {playerig.legs},
            feet = {playerig.feet},
            head = {playerig.head},
            neck = {playerig.neck},
            finger1 = {playerig.finger1},
            finger2 = {playerig.finger2},
            finger3 = {playerig.finger3},
            finger4 = {playerig.finger4},
            earleft = {playerig.earleft},
            earright = {playerig.earright},
            primaryw = {playerig.primaryw},
            secondary = {playerig.secondary},
            range = {playerig.range},
            zone_id = {playerig.zone_id},
            melee = {playerig.melee},
            spells = {playerig.spells},
            diety = "{playerig.diety}",
            dietylore = "{playerig.dietylore}",
            ac = {playerig.ac}
            WHERE Name like '{playerig.name}';""")
    conn.commit()

#Things to figure out:
#how to assign those printed variables to the correct variable in the player class (playerig)
#how to assign the id to the it option == command. select the [0] and [1] etc for the loop?




###################################################################################
#                   Player Class
###################################################################################
class player:
    def __init__(self, name):
        self.id = int()
        self.name = name
        self.level = 1
        self.pclass = int()
        self.race = int()
        self.maxhealth = int()
        self.health = int()
        self.maxmana = int()
        self.mana = int()
        self.potions = int()
        self.exp = int()
        self.levelupxp = int()
        self.str = int()
        self.sta = int()
        self.agi = int()
        self.dex = int()
        self.cha = int()
        self.int = int()
        self.wis = int()
        self.gold = int()
        self.resist = int()
        self.quest = ''
        self.inventory = ''
        self.chest = int()
        self.arms = int()
        self.wrist = int()
        self.hands = int()
        self.legs = int()
        self.feet = int()
        self.head = int()
        self.neck = int()
        self.finger1 = int()
        self.finger2 = int()
        self.finger3 = int()
        self.finger4 = int()
        self.earleft = int()
        self.earright = int()
        self.primaryw = int()
        self.secondary = int()
        self.range = int()
        self.zone_id = int()
        self.melee = int() 
        self.spells = int()
        self.diety = ''
        self.dietylore = ''
        self.ac = int()
        self.meleeskills = int()
        self.spellskills = int()
        self.meleeskillup = int()
        self.spellskillup = int()
        self.MSXP = int()
        self.SSXP = int()


###################################################################################
#                   Enemies (though this really should be in a DB)
###################################################################################
#create a local DB and make an ememies table, print name column or whatever values we need, hp etc

class enemy:
    def __init__(self, name):
        self.id = int()
        self.name = name
        self.maxhealth = int()
        self.health = self.maxhealth
        self.maxmana = int()
        self.mana = self.maxmana
        self.goldgain = int()
        self.str = int() 
        self.sta = int()
        self.agi = int()
        self.dex = int()
        self.cha = int()
        self.int = int()
        self.wis = int()
        self.resist = int()
        self.itemdrop = int()
        self.melee = int()
        self.spells = int()
        self.expgain = int()
        self.ac = int()







###################################################################################
#                   Define Game variables
###################################################################################

playerig = None

def osdetect():
    global tclear
    platform = sys.platform
    tclear = ''
    platform
    if platform == 'darwin':
        tclear = 'clear'
        start()
    elif platform == 'linux' or platform == 'linux32':
        tclear = 'clear'
        start()
    elif platform == 'win32':
        tclear = 'cls'
        start()
    else:
        print("Cannot determine what operating system you're running on. \nThis Application can run on linux, windows, and mac only")
        print("If you're running on a toaster,  please update to toaster2.0 running debian to play")
        input("press enter to exit the program and fix the error")
        sys.exit()

def start():
    os.system('clear')
    global playerig
    playerig = player("")
    print("Welcome to the world of Norrath, adventurer.\n")
    option = input("Select a name for yourself\n>")
    playerig = player(option)
    #playerig.name = option
    namecheck()

def namecheck():
    #UBUNTU
    #conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
    #OS X
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    try:
        with conn:
            conn.execute(f"""INSERT INTO PLAYER (Name, level, pclass, race, maxhealth, health, maxmana, mana, potions, exp, levelupxp, str, sta, agi, dex, cha, int, wis, gold, resist, quest, inventory, chest, arms, wrist, hands, legs, feet, head, neck, finger1, finger2, finger3, finger4, earleft, earright, primaryw, secondary, range, zone_id, melee, spells)
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?) """, (playerig.name, playerig.level, playerig.pclass, playerig.race, playerig.maxhealth, playerig.health, playerig.maxmana, playerig.mana, playerig.potions, playerig.exp, playerig.levelupxp, playerig.str, playerig.sta, playerig.agi, playerig.dex, playerig.cha, playerig.int, playerig.wis, playerig.gold, playerig.resist, playerig.quest, playerig.inventory, playerig.chest, playerig.arms, playerig.wrist, playerig.hands, playerig.legs, playerig.feet, playerig.head, playerig.neck, playerig.finger1, playerig.finger2, playerig.finger3, playerig.finger4, playerig.earleft, playerig.earright, playerig.primaryw, playerig.secondary, playerig.range, playerig.zone_id, playerig.melee, playerig.spells))
            
            conn.commit()
    except sqlite3.IntegrityError:
        print("Record Already Exists")
        input("Press Enter to try a new name")
        start()
    finally:
        print(f"Welcome {playerig.name}!")
        input("Press Enter to select a race")

    conn.commit()
    conn.close()
    newsavetest()

def loadcheck():
    pass
#in the database, set each race to a specific ID. then set the player.race to INT() and then build a query to call the array and the classes available to it. 
# lots of database mapping needed to shorted this out.
#this needs to be changed to call the class table when i remake that to use scripts to assign the variable values.

def newsavetest():
    racelist = (1, 2, 3, 4, 5, 6, 7) #I don't think i need these anymore.
    classlist = (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14)
    print("Please Choose your Race")
    roption = input("1. Human\n2. Ogre\3. Wood Elf\n4. Dwarf\n5. High Elf\n6. Troll\n7. Dark Elf\n>")
    #for roption in racelist:
    #    for row in c.execute(f"SELECT id from RACE where id = {roption}"):
    #        playerig.race = row[0]
    playerig.race = roption
    for row in c.execute(f"SELECT str, sta, agi, dex, cha, int, wis, resist, diety, dietylore FROM RACE WHERE id = {playerig.race}"):
        playerig.str += int(row[0])
        playerig.sta += int(row[1])
        playerig.dex += int(row[3])
        playerig.agi += int(row[2])
        playerig.cha += int(row[4])
        playerig.int += int(row[5])
        playerig.wis += int(row[6])
        playerig.resist += int(row[7])
        playerig.diety = row[8]
        playerig.dietylore = row[9]
        
    print("SELECT A CLASS")
    coption = input("1. Warrior\n2. Shadow Knight\n3. Paladin\n4. Druid\n5. Cleric\n6. Shaman\n7. Necromancer\n8. Rogue\n9. Ranger\n10. Magician\n11. Monk\n12. Enchanter\n13. Bard\n14. Wizard\n> " )
    #for coption in classlist:
    #    for row in c.execute(f"SELECT id FROM PCLASS where id = {coption}"):
    #        playerig.pclass = row[0]
    playerig.pclass = coption
    for row in c.execute(f"SELECT str, sta, agi, dex, cha, int, wis, resist, ac FROM PCLASS WHERE id = {playerig.pclass}"):
        playerig.str += int(row[0])
        playerig.sta += int(row[1])
        playerig.dex += int(row[3])
        playerig.agi += int(row[2])
        playerig.cha += int(row[4])
        playerig.int += int(row[5])
        playerig.wis += int(row[6])
        playerig.resist += int(row[7])
        playerig.ac = int(row[8])
        
    
    print("Character creation has been staged. Please wait a moment while we load the rest of the attributes")
    time.sleep(2)
    playerig.primaryw = 1
    playerig.health = playerig.sta * 2
    playerig.maxhealth = playerig.sta * 2
    playerig.levelupxp = 300
    playerig.meleeskills = 1
    playerig.spellskills = 1
    playerig.melee = 1 
    playerig.spells = 1
    playerig.meleeskillup = 20
    playerig.spellskillup = 20
    playerig.MSXP = 0
    playerig.SSXP = 0
    input("Success! Press Enter to continue")
    playersave()



#add stat changes
def playersave():
    #UBUNTU
    #conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
    #OS X
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    cursorObj = conn.cursor()
    cursorObj.execute(f"""UPDATE PLAYER 
        SET level = {playerig.level},
        pclass = {playerig.pclass},
        race = {playerig.race},
        maxhealth = {playerig.maxhealth},
        health = {playerig.health},
        maxmana = {playerig.maxmana},
        mana = {playerig.mana},
        potions = {playerig.potions},
        exp = {playerig.exp},
        levelupxp = {playerig.levelupxp},
        str = {playerig.str},
        sta = {playerig.sta},
        agi = {playerig.agi},
        dex = {playerig.dex},
        cha = {playerig.cha},
        int = {playerig.int},
        wis = {playerig.wis},
        gold = {playerig.gold},
        resist = {playerig.resist},
        quest = "{playerig.quest}",
        inventory = "{playerig.inventory}",
        chest = {playerig.chest},
        arms = {playerig.arms},
        wrist = {playerig.wrist},
        hands = {playerig.hands},
        legs = {playerig.legs},
        feet = {playerig.feet},
        head = {playerig.head},
        neck = {playerig.neck},
        finger1 = {playerig.finger1},
        finger2 = {playerig.finger2},
        finger3 = {playerig.finger3},
        finger4 = {playerig.finger4},
        earleft = {playerig.earleft},
        earright = {playerig.earright},
        primaryw = {playerig.primaryw},
        secondary = {playerig.secondary},
        range = {playerig.range},
        zone_id = {playerig.zone_id},
        melee = {playerig.melee},
        spells = {playerig.spells},
        diety = "{playerig.diety}",
        dietylore = "{playerig.dietylore}",
        ac = {playerig.ac},
        meleeskills = {playerig.meleeskills},
        spellskills = {playerig.spellskills},
        meleeskillup = {playerig.meleeskillup},
        spellskillup = {playerig.spellskillup},
        MSXP = {playerig.MSXP},
        SSXP = {playerig.SSXP}
        WHERE Name like '{playerig.name}';""")

    conn.commit()
    conn.close()
    print("You have successfully created your character")
    input("Press Enter to continue")
    intro()


###################################################################################
#                   Post Create/Load Screen
###################################################################################
###    TODO   ###
#Do I keep intro and first quest in savegame? does that make sense? 
def intro():
    os.system('clear')
    print("Welcome to the world of Norrath. Prepare yourself as you embark on a journey full of danger, mystery and reward.")
    time.sleep(1)
    print("If you are brave enough,")
    time.sleep(1)
    print("strong enough,")
    time.sleep(1)
    print("and determined enough, you will reap incredible treasures beyond your wildest dreams.")
    time.sleep(1)
    print("Be warned, in this world you are not a hero. You are not a king, an unbeatable, legendary knight.")
    time.sleep(1)
    print("You are no one, and if you accept this fact, it will be your greatest strength")
    print(f"{playerig.name}, you are a {playerig.race}, you are in {playerig.zone_id}")
    #maybe add this into another introcall function and call that based on race
    #eventually go to zone intro function -- for now, 
    input("\n\nPress Enter to continue")
    firstquest()
    


def firstquest(): #This is a generic intro quest, but will be replaced with specific starter quests once the zones are made
    os.system('clear')
    print("You find yourself standing in front of a large creature. This creature has blue skin, 10 eyes, and talons for hands.\n")
    print("He... it... is wearing a long black robe with a white outline going down to his feet.\n")
    print("It looks at you, knowingly.\n")
    print("'Welcome to the void young adventurer.' It says.\n")
    print("You notice you can't move your body.\n")
    print("'I have locked you here in space and time to deliver a warning. I have attempted to warn countless other, but they do not listen.' His voice is deep and powerful, you want to brace yourself but are unable to.\n")
    print("'Long ago, the great crystaline dragon, ruler of the Plane of Sky, delivered her brood to a lifeless planet.\n") 
    print("With one swipe of her mighty claw she lay claim to the world now known as Norrath. The other gods noticed her actions and decided to create their own followers to attempt to conquer the world.''n")
    print(f"As a {playerig.race}, you worship the god known as {playerig.diety}.{playerig.dietylore}\n")
    print("One of the Crystaline Dragon's followers Kerafyrm, The Sleeper is the most powerful known being in Norrath. The great prismatic dragon was born out of the forbidden coupling of two different elemental dragons - making him into a great abomination in the eyes of many. At first many dragons flocked to Kerafyrm feeling the god-like being would bring a new age to the children of Veeshan. However, his unfathomable power and the taboo of his creation by the laws of Veeshan led to a great madness overwhelming Kerafyrm. The laws of Veeshan forbid dragons to kill dragons under any circumstance and Kerafyrm violated these laws too by killing those who spoke out against him. \n")
    print("His fanatics were driven away and in a great struggle the first brood subdued him into a great sleep and sealed him within Sleeper's Tomb. Four dragons were left to perpetually guard the seal. Sadly, as ages have passed the first brood are just a handful. Some dragons have come to question the wisdom of sealing him away - The Shade of Jaled'Dar included.\n")
    print("You must stop The Sleeper from awakening. Find the fanatics and put an end to their plan to awaken Kerafyrm and keep him in his eternal tomb.\n")
    print("But first, you need a weapon.")
    print("You receive a Rusty Sword") #this will be pulled from the database based on class later
    input("> ")
    
    #how to i make this go back to nevertext.py

###################################################################################
#                   Player Stats
###################################################################################

def topmenu():
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    for row in conn.execute(f"SELECT PCLASS.classname FROM PCLASS JOIN PLAYER ON PCLASS.id = PLAYER.pclass WHERE Name = '{playerig.name}'"):
        pclass = row[0]
    #pclass = row[0]
    os.system('clear')
    healthDashes = 10
    #runs this command in terminal
    print(f"Name:  {playerig.name:<10}\t|\tStamina: {playerig.sta:<10}\t\tExperience: {playerig.exp}\t|Next Level: {playerig.levelupxp}")
    print(f"Class: {pclass:<10}")
    print(f"Level: {playerig.level:<10}\t|\tAgility: {playerig.agi:<10}\t|Charisma: {playerig.cha}")
    print(f"Gold:    {playerig.gold:<10}\t|\tStrength: {playerig.str:<10}\t|Intelligence: {playerig.int}")
    print(f"Potions: {playerig.potions:<10}\t|\tDexterity: {playerig.dex:<10}\t|Wisdom: {playerig.wis:<10}")
    print(f"Health   {playerig.health}/{playerig.maxhealth:<10}\t|\tMana: {playerig.mana} / {playerig.maxmana}")
    dashConvert = int(playerig.maxhealth/healthDashes)                         
    currentDashes = int(playerig.health/dashConvert)                           
    remainingHealth = healthDashes - currentDashes                    

    healthDisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingHealth)]) 
    percent = str(int((playerig.health/playerig.maxhealth)*100)) + "%"                 

    print("|" + healthDisplay + remainingDisplay + "|")               
    print("     " + percent) 
    print(f"Spell: {playerig.spells}")
    print("Combat Stats:")
    print(f"Melee Skill: {playerig.meleeskills:<10}\t|\tMelee Skill Exp: {playerig.MSXP:<10}\t| Melee Xp to level: {playerig.meleeskillup}")
    print(f"Spell SKill: {playerig.spellskills:<10}\t|\tSpell Skill Exp: {playerig.SSXP:<10}\t| Spell Xp to level: {playerig.spellskillup}")
    print("--------------------------------------------------------------------------------------------------\n")
    print("Currently Equiped Items:")
    print(f"Head: {playerig.head}")
    print(f"Left Ear: {playerig.earleft} | Neck: {playerig.neck} | Right Ear: {playerig.earright}")
    print(f"Arms: {playerig.arms} | Chest: {playerig.chest} | Wrist: {playerig.wrist}")
    print(f"Hands: {playerig.hands} | Legs: {playerig.legs} | Feet: {playerig.feet}")
    print(f"Finger1 : {playerig.finger1} | Finger2: {playerig.finger2} | Finger3: {playerig.finger3} | Finger4: {playerig.finger4}")
    print(f"Primary: {playerig.primaryw} | Secondary: {playerig.secondary} | Range: {playerig.range}")
    print("====================================================================================================")


#Do i need level up in here or in nevertext? this isn't being saved to the database yet, it could be done in the main function...
#Though if i end up pulling numbers from a level up table, it would make more sense to keep here
# let's keep it here
def levelup():
    os.system('clear')
    topmenu()
    playerig.level += 1
    playerig.maxhealth *= 1.25
    playerig.maxhealth = round(playerig.maxhealth)
    playerig.health = playerig.maxhealth
    playerig.maxmana *= 1.25
    playerig.maxmana = round(playerig.maxmana)
    playerig.mana = playerig.maxmana
    playerig.levelupxp *= 2.5
    print("DING!\n")
    print("You have gained a level!")
    print(f"You are now level {playerig.level}!")
    print("Your health and mana have increased")
    input("Press Enter to continue")
    
    #this is going to be called a lot, needs to be efficient
    #need to define the levelupxp in the database by level and also add a modifier based on class/race combo?


###################################################################################
#                   load function
###################################################################################

#add logic to check name with database and if no match go back to load()
def load():
    global playerig
    playerig = player("")
    #conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    #sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
    #c.execute("SELECT Name FROM PLAYER;")
    for row in c.execute("SELECT Name, level, race, pclass FROM PLAYER;"):
        print(row)
    pname = input("Please Enter the name of the character you want to load.\n>")
    #print("Please select saved game by typing in the number you want to load")
    c.execute(f"SELECT * FROM PLAYER WHERE Name like '{pname}';")
    data = c.fetchall()
    #print(data)
    for row in data:
        print(row)
    input("> ")
    playerig.id = row[0]
    playerig.name = row[1]
    playerig.level = row[2]
    playerig.pclass = row[3]
    playerig.race = row[4]
    playerig.maxhealth = row[5]
    playerig.health = row[6]
    playerig.maxmana = row[7]
    playerig.mana = row[8]
    playerig.potions = row[9]
    playerig.exp = row[10]
    playerig.levelupxp = row[11]
    playerig.str = row[12]
    playerig.sta = row[13]
    playerig.agi = row[14]
    playerig.dex = row[15]
    playerig.cha = row[16]
    playerig.int = row[17]
    playerig.wis = row[18]
    playerig.gold = row[19]
    playerig.resist = row[20]
    playerig.quest = row[21]
    playerig.inventory = row[22]
    playerig.chest = row[23]
    playerig.arms = row[24]
    playerig.wrist = row[25]
    playerig.hands = row[26]
    playerig.legs = row[27]
    playerig.feet = row[28]
    playerig.head = row[29]
    playerig.neck = row[30]
    playerig.finger1 = row[31]
    playerig.finger2 = row[32]
    playerig.finger3 = row[33]
    playerig.finger4 = row[34]
    playerig.earleft = row[35]
    playerig.earright = row[36]
    playerig.primaryw = row[37]
    playerig.secondary = row[38]
    playerig.range = row[39]
    playerig.zone_id = row[40]
    playerig.melee = row[41]
    playerig.spells = row[42]
    playerig.diety = row[43]
    playerig.dietylore = row[44]
    playerig.ac = row[45]
    playerig.meleeskills = row[46]
    playerig.spellskills = row[47]
    playerig.meleeskillup = row[48]
    playerig.spellskillup = row[49]
    playerig.MSXP = row[50]
    playerig.SSXP = row[51]
    
    print("Loaded successfully")
    input("Press Enter to continue...")
    





###MAIN FUNCTION HERE? if this is a called script from nevertext.py then this remains blank