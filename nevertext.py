###################################################################################
#                   IMPORTS
###################################################################################

from __future__ import division
from __future__ import absolute_import
import os
import sys
import sqlite3 
from os import system
import random
import json
import time
from packages import osdetect
from packages import charactercreate
from packages import tutorial
from packages import savegame
from quests import Sir_Lucans_rise

#from .package.subpackage1.tutorial import tut_help
#from package import *
#from package.tutorial import tut_help



###################################################################################
#                   Database Code
###################################################################################
#init connection (always active, or should i call this?)
#this needs to be in the OS check function to allow me to define where the database is saved..
#there has to be a better way.
#UBUNTU
#conn = sqlite3.connect('/home/guindel/Documents/nevertext/Database/nevertext.db')
#OS X
conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
c = conn.cursor()
#def save should not have the save prompt. that should be a pre-save function so i can dynamically call
###################################################################################
#                   Main function (Player sees this first)
###################################################################################

def computercheck():
    """
    Checks for operating system and assigns all relevant variables to the os.system command
    Once that's done and it's successful, starts the game
    """
    osdetect.osdetect()
    main()



def main():#move the tclear function to another function

    os.system('clear')
    
    print("###################################################################################")
    print("                     NeverText - An EverQuest Inspired Game")
    print("                            Version 0.0.4")
    print("###################################################################################")
    print("Please make a selection below\n ")
    print("1. Start a new game")
    print("2. Load a saved game")
    print("3. Tutorial")
    print("4. Leave the game")
    option = input("> ")
    if option == "1":
        savegame.start()
        start1()
    elif option == "2":
        savegame.load()
        start1()
    elif option == "3":
        #need to figure out how to import this properly
        tutorial.tut_help()
    elif option == "4": 
        sys.exit()
    else:
        print("Please select an option above")
        main()





###################################################################################
#                   Define Game variables
###################################################################################

savegame.playerig = None

def start():
    #This will be the new start function  after main once i get other things solidified
    pass



###################################################################################
#                   Main Menu
###################################################################################


def start1():
    os.system('clear')
    savegame.topmenu()
    #print f formats the print so i can put the variables inside curly brackets
    print("\nWhat would you like to do?\n")
    print("###############################")
    print("1. Fight")
    print("2. Store")
    print("3. Inventory")
    print("4. Quests")
    print("5. Save")
    print("6. Exit")
    print("A. Admin_Commands (for testing)")
    print("###############################")
    option = input("> ")

    if option == "1":
        prefight()
    elif option == "2":
        store()
    elif option == "3":
        inventory()
    elif option == "4":
        quest()
    elif option == "5":
        savegame.save()
        start1()
    elif option == "6":
        sys.exit()
    elif option == "A":
        admin()
    else:
        start1()


###################################################################################
#                   Combat Screen / Functions
###################################################################################
#pull from savegame?
#put into a for loop and dynamically assign based on a rand call. assign id number to list rand that then call the id?
def prefight():
    global enemyig
    os.system('clear')
    c.execute('SELECT * FROM ENEMY;')#This will not work long term, but since i have one enemy in the database for testin, it works now.
    data = c.fetchall()
    #print(data)
    for row in data:
        print(row)
    option = input("> ")
    enemyig = savegame.enemy(option)
    enemyig.id = row[0]
    enemyig.name = row[1]
    enemyig.maxhealth = row[2]
    enemyig.health = row[3]
    enemyig.maxmana = row[4]
    enemyig.mana = row[5]
    enemyig.goldgain = row[6]
    enemyig.str = row[7]
    enemyig.sta = row[8]
    enemyig.agi = row[9]
    enemyig.dex = row[10]
    enemyig.cha = row[11]
    enemyig.int = row[12]
    enemyig.wis = row[13]
    enemyig.resist = row[14]
    enemyig.itemdrop = row[15]
    enemyig.melee = row[16]
    enemyig.spells = row[17]
    enemyig.expgain = row[18]
    os.system('clear')
    print(f"You have been attacked by {enemyig.name}!")
    input("Press ENTER to begin")
    fight()


    #add random number selection from database where zone id = current zone id
    #select random enemy
    #print to screen with all stats
    #print("you have encountered a")
    ###I would love to have it select a random statement like one could be #you have encountered a, and the other is you were attacked by a and so on
    #need to build table in database and add some enemies first. 


#todo:
#add enemy level to encounter text
# look into splitting the encounter into encounter() that follows prefight and leads into fight()
#
def fight():
    savegame.topmenu()
    healthDashes = 20 
    #os.system('clear')
    #print(f"{savegame.playerig.name}: Health: {savegame.playerig.health} / {savegame.playerig.maxhealth} | {enemyig.name}: Health{enemyig.health} / {enemyig.maxhealth}")
    #print(f"Mana: {savegame.playerig.mana} / {savegame.playerig.maxmana}")
    #print(f"Potion: {savegame.playerig.potions}")
    #print(f"1. {savegame.playerig.melee}\n {savegame.playerig.spells}")
    print(f"{enemyig.name}\nHealth: {enemyig.health} / {enemyig.maxhealth}\n")
    dashConvert = int(enemyig.maxhealth/healthDashes)   #HP BAR!!!            
    currentDashes = int(enemyig.health/dashConvert)                           
    remainingHealth = healthDashes - currentDashes                    

    healthDisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingHealth)]) 
    percent = str(int((enemyig.health/enemyig.maxhealth)*100)) + "%"                 

    print("|" + healthDisplay + remainingDisplay + "|")               
    print("         " + percent) 
    print("What will you do?")
    option = input(f"1. Attack\n2. Drink Potion\n3. Run\n> ")
    if option == "1":
        attackmelee()
    elif option == "2":
        drinkpotion()
    elif option == "3":
        run()
    else:
        fight()

#needs to build out spell table and assign id to player and enemies (shared resource)
def attackspell():
    #In progress - NOT WORKING
    '''
    global spellattack
    for row in  c.execute(f"SELECT damage from SPELLS where id = 1;"):
        s = row[0]
    spelldmg = int(s)
    '''
    pass


def attackmelee():#PRE-PRE-Alpha
    global meleeatk
    for row in c.execute(f"SELECT damage from ITEMS where id = {savegame.playerig.primaryw};"):
        a = row[0]
    weapondmg = int(a)
    #c.execute(f"SELECT PLAYER.Name, PLAYR.dex, MELER.str, PLAYEEE.damage, MELEE.atkname FROM PLAYER JOIN MELEE ON PLAYER.primaryw = MELEE.id;")
    c.execute(f"SELECT MELEE.damage, MELEE.atkname FROM PLAYER JOIN MELEE ON PLAYER.primaryw = MELEE.id WHERE PLAYER.Name = '{savegame.playerig.name}';")
    z = c.fetchone()[0]
    #for row in data:
    #    print(row)
    meleedamage = int(z)
    c.execute(f"SELECT MELEE.damage, MELEE.atkname FROM PLAYER JOIN MELEE ON PLAYER.primaryw = MELEE.id WHERE PLAYER.Name = '{savegame.playerig.name}';")
    y = c.fetchone()[1]
    meleeatk = y
    meleeatk = "Primary Weapon Attack"
    spellatk = "Primary Spell Attack"
    print(f"1. Melee: {meleeatk}")
    print(f"2. Spell: {spellatk}")
    option = input("> ")
    #calculations for melee is wrong. it's combining melee damage from the skill with weapon damage which should be separate calculations..
    #I'm going to need to redo all of the combat calculations here in order to have some working model
    if option == "1":
        d1calc = savegame.playerig.str + weapondmg + savegame.playerig.dex 
        defcalc = (enemyig.agi + enemyig.sta) * 0.40
        damage = round((d1calc - defcalc) * 1.10)
        #damage = (((meleedamage + savegame.playerig.str + savegame.playerig.dex + savegame.playerig.melee + weapondmg) % (enemyig.agi + enemyig.sta))) + random.randint(1, 6)
        if damage <= 0:
            damage = 0
        if damage != 0:
            savegame.playerig.MSXP += 5
        if savegame.playerig.MSXP >= savegame.playerig.meleeskillup:
            savegame.playerig.MSXP = 0
            savegame.playerig.meleeskills +=1
            savegame.playerig.meleeskillup *= 1.5
            savegame.playerig.meleeskillup = round(savegame.playerig.meleeskillup)
            print(f"You have gotten better at melee!({savegame.playerig.meleeskills})")
        print(f"You hit {enemyig.name} for {damage} points of Damage.")
        enemyig.health -= damage
        if enemyig.health <= 0:
            input("> ")
            win()    
        input("> ")
        #this modified combat system is better, but the enemy does a lot of damage now. need to adjust stats for lower levels
        #when player gets items, or is at a level that's expected to get items, then we can add more damage and stats to enemy
        ed1calc = enemyig.str + enemyig.dex
        edefcalc = (savegame.playerig.agi + savegame.playerig.sta + savegame.playerig.ac) * 0.40
        Edamage = round((ed1calc - edefcalc) * 1.10)
        #Edamage = (enemyig.str + enemyig.dex) % savegame.playerig.agi
        if Edamage <= 0:
            Edamage = 0
        savegame.playerig.health -= Edamage
        print(f"You were hit for {Edamage} points of damage")
        input("> ")
        if savegame.playerig.health <= 0:
            time.sleep(1)
            dead()
        else:
            fight()
    elif option == "2":
        print(f"You cast a spell on {enemyig.name}")
        sdamage = round(((savegame.playerig.int + savegame.playerig.wis + savegame.playerig.spells) - (enemyig.agi + enemyig.sta)) + random.randint(1, 6))#this needs to be fixed
        if sdamage <= 0:
            sdamage = 0
        if sdamage != 0:
            savegame.playerig.SSXP += 5
        if savegame.playerig.SSXP >= savegame.playerig.spellskillup:
            savegame.playerig.SSXP = 0
            savegame.playerig.spellskills += 1
            savegame.playerig.spellskillup *= 1.5
            savegame.playerig.spellskillup = round(savegame.playerig.spellskillup)
            print(f"You have gotten better at magic!({savegame.playerig.spellskills})")
        print(f"You hit {enemyig.name} for {sdamage} points of damage.")
        enemyig.health -= sdamage
        if enemyig.health <= 0:
            input("> ")
            win()
        input("> ")
        ed1calc = enemyig.str + enemyig.dex
        edefcalc = savegame.playerig.agi + savegame.playerig.sta + savegame.playerig.ac
        Edamage = round((ed1calc - edefcalc) * 1.10)
        if Edamage <= 0:
            Edamage = 0
        savegame.playerig.health -= Edamage
        print(f"You were hit for {Edamage} points of damage")
        time.sleep(1)
        if savegame.playerig.health <= 0:
            time.sleep(1)
            dead()
        else:
            fight()
    else:
        attackmelee()
    #This is the basic logic for pulling the relevant data to calculate the attack damage of a player. 
    #eventually need to add a random to calculate the damage so it's changing and not always the same each time. 
    #assign all variables from the data to the relevant options and then calculate the damage for both player and enemy. 


    #playeratk = conn.execute(f"SELECT {savegame.playerig.name}, {savegame.playerig.str}, {savegame.playerig.dex}, {melee.damage} FROM PLAYER JOIN MELEE ON PLAYER.primaryw = MELEE.id;")
    #this whole section needs reworked with the math functions
    #I removed the "attack" veriables and replaced them with player stats, exmaple thoughts:
    #hit = ((savegame.playerig.str + weapon.attack + savegame.playerig.dex) / ((enemy.sta + enemy.agi + enemy.resist) * 0.5) 0.5 is arbitrary example
    #final = round(random.uniform(hit), 0)


def run():
    os.system('clear')
    runrun = random.uniform(0, 5)
    if runrun >= 3:
        print("you have successfully run away")
        input("Press Enter")
        start1()
    else:
        print("you have failed to run away")
        input("press enter")
        ed1calc = enemyig.str + enemyig.dex
        edefcalc = savegame.playerig.agi + savegame.playerig.sta + savegame.playerig.ac
        Edamage = round((ed1calc - edefcalc) * 1.10)
        if Edamage <= 0:
            Edamage = 0
        savegame.playerig.health -= Edamage
        print(f"You were hit for {Edamage} points of damage")
        time.sleep(1)
        if savegame.playerig.health <= 0:
            time.sleep(1)
            dead()
        fight()

def drinkpotion():
    os.system('clear')
    if savegame.playerig.potions >= 1:
        savegame.playerig.health += 50
        savegame.playerig.potions -= 1
        if savegame.playerig.health > savegame.playerig.maxhealth:
            savegame.playerig.health = savegame.playerig.maxhealth
        print("you drank a potion, it restored health")
        print("> ")
        fight()
    else:
        print("you don't have any potions!")
        input("> ")
        fight()

def win():
    os.system('clear')
    savegame.topmenu()
    print(f"you have defeated an {enemyig.name}!")
    print(f"You have gained {enemyig.expgain} experience points!")
    print(f"You have looted {enemyig.itemdrop}") #need to add this to the logic below and the database
    input("Press ENTER to continue")
    savegame.playerig.exp += enemyig.expgain
    savegame.playerig.gold += enemyig.goldgain
    if savegame.playerig.exp >= savegame.playerig.levelupxp:
        levelup()
    else:
        start1()


def dead():
    savegame.playerig.health = savegame.playerig.maxhealth
    print("You have died")
    print("you will be returned to town")
    input("\nPress Enter to revive yourself at the nearest town ")
    start1()

###################################################################################
#                   Stores
###################################################################################

def store():
    print("Welcome to the pre-alpha store")
    print("Please take a look around")
    option = input("1. Weapons\n2. Armor\n3. Books\4. Leave")
    if option == "1":
        print("weapons aren't in the shop yet")
        input("Press enter to continue")
        start1()
    elif option == "2":
        print("Armor pieces aren't in the shop yet")
        input("Press enter to continue")
        start1()
    elif option == "3":
        print("Books aren't in the shop yet")
        input("Press enter to continue")
        start1()
    elif option == "4":
        print("Thanks for visiting")
        input("Press enter to continue")
        start1()
    else:
        store()





###################################################################################
#                   Character Management
###################################################################################


def inventory():
    os.system('clear')
    savegame.topmenu()
    print("############ Inventory ############")
    print("What do you want to see?")
    option = input("1. Weapons\n2. Armor\n3. Misc\n")
    if option == "1":
        print("Weapons")
        input("> ")
        start1()
    elif option == "2":
        print("Armor")
        input("> ")
        start1()
    elif option == "3":
        print("misc")
        input("> ")
        start1()
    else:
        inventory()
    

def quest():
    os.system('clear')
    savegame.topmenu()
    print("Welcome to the Quest menu")
    print("Currently not implemented.")
    input("press ENTER to return to the main menu")
    start1()

def admin():
    os.system('clear')
    savegame.topmenu()
    print("Admin Panel:")
    print("This is used for setting specific values for testing purposes")
    print("If you see this menu and option in the game you're playing, you don't have the latest live version")
    print("Please navigate to [website] to get the latest verison")
    print("What would you like to do:")
    print("1. Increase player level by 1")
    print("2. Add 1000 gold to player")
    print("3. add potion to player")
    print("4. Improve stats")
    print("5. Heal")
    print("6. leave")
    option = input("> ")
    if option == "1":
        savegame.playerig.level += 1
        admin()
    elif option == "2":
        savegame.playerig.gold += 1000
        admin()
    elif option == "3":
        os.system('clear')
        savegame.topmenu()
        option = input("Press Enter to add a option")
        savegame.playerig.potions += 1
        admin()
    elif option == "4":
        os.system('clear')
        savegame.topmenu()
        print("which stat do you want to improve?")
        print("1. str\n2. sta\n3. agi\n4. dex\n5. cha\n6. int\n7. wis")
        option = input("> ")
        if option == "1":
            savegame.playerig.str += 2
            admin()
        elif option == "2":
            savegame.playerig.sta += 2
            admin()
        elif option == "3":
            savegame.playerig.agi += 2
            admin()
        elif option == "4":
            savegame.playerig.dex += 2
            admin()
        elif option == "5":
            savegame.playerig.cha += 2
            admin()
        elif option == "6":
            savegame.playerig.int += 2
            admin()
        elif option == "7":
            savegame.playerig.wis += 2
            admin()
        else:
            admin()
    elif option == "5":
        savegame.playerig.health = savegame.playerig.maxhealth
        admin()
    elif option == "6":
        start1()
    else: 
        admin()

    
def spells():
    os.system('clear')
    savegame.topmenu()
    print("Spells Section")
    print("This is not currently implemented")
    input("Press ENTER to return to the previous screen")
    start1()


def levelup():
    os.system('clear')
    savegame.topmenu()
    savegame.playerig.level += 1
    savegame.playerig.maxhealth *= 1.25
    savegame.playerig.maxhealth = round(savegame.playerig.maxhealth)
    savegame.playerig.health = savegame.playerig.maxhealth
    savegame.playerig.maxmana *= 1.25
    savegame.playerig.maxmana = round(savegame.playerig.maxmana)
    savegame.playerig.mana = savegame.playerig.maxmana
    savegame.playerig.levelupxp *= 2.5
    print("DING!\n")
    print("You have gained a level!")
    print(f"You are now level {savegame.playerig.level}!")
    print("Your health and mana have increased")
    input("Press Enter to continue")
    start1()
    #this is going to be called a lot, needs to be efficient
    #need to define the levelupxp in the database by level and also add a modifier based on class/race combo?




###################################################################################
#                   Group Management
###################################################################################
# This is going to require a LOT of rework of the game as a whole
def group():
    pass

def groupadd():
    pass

def groupremove():
    pass

def groupsay():
    #This will be for fun chat messages within combat and travel
    pass

def groupdie():
    pass

def ally():
    pass
    #This will be for when a group member has decided to be a permanent ally option
    #example: you did something that a specific race likes enough to ally with you
    # allies will have a stronger bond and a +15% boost to their stats and +5% to yours
    #this will be complex
###################################################################################
#                   load function
###################################################################################


computercheck()