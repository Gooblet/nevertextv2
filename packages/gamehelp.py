import os
import sys
from . import osdetect


def starthelp():
    osdetect.clear
    print("Welcome to the world of Norrath")
    print("I am Feronia Vie, I am here to guide you on the start of your journey through Norrath")
    print("A few things to get out of the way before we begin:")
    print("1. This game is heavily inspired by EverQuest. Originally developed by 989 studios, absorbed into Sony Online Entertainment, then bought by Daybreak Games. As such, this game will never be for money or donations, this is strictly fan made. I will let the creator speak yo you for a moment")
    print("Greetings player - I go by Guindel in Norrath's world of EverQuest, you may encounter me, a fellow adventurer in the game at some point. I wanted to say thank you for checking out my game. I made this with 2 main purposes. 1. I wanted to get better at programming. 2. I love EverQuest lore and feel the former and current players would get some enjoyment about learning more about the lore of Norrath through a game. This is currently in pre alpha and it will be here for a while. As such, there will be bugs, and i will eventually fix them, but since this is just a hobby, I will make no guarantees as to when things will be fixed. You can definitely let me know of bugs you encounter through gitlab and i will make a note of them to fix when i get to that section of the code. If you want to just read tons of lore, and not play the game, clone the repository and look in the 'LORE' Folder for all the text documents. Other than that - enjoy!")
    print("2. Alright, now that he is out of the way, let's move on. This is a text based game. In its current state the game in progressed by selecting the number or letter option given to you. Let's try this:")
    option = input("1. Progress forward\n2. don't push number 2")
    if option == "1":
        print("well done, you understand how numbers work and are capable of reading common tounge at a basic level")
        starthelp1()
    else:
        print("... let's start over. read carefully. in the future the punishment won't be so forgiving...")
        starthelp()

def starthelp1():
    print("Now, let's take a look at some of the things you can explore in the world of norrath")
    option = input("1. What is EverQuest/NeverText\n2. What races are there?\n3. What classes are there?\n4. What is the current future development of the game?\n5. Return to the main game (currently quits game. just rerun)")
    if option == "1":
        eqntlore()
    elif option == "2":
        racelore()
    elif option == "3":
        classlore()
    elif option == "4":
        devprogress()
    else:
        sys.exit()

def eqntlore():
    print("Let's start with EverQuest\nDeveloped and relased to the public March 16th 1999, EverQuest was the worlds first True 3D MMORPG.\nIt was ... it was truly an amazing game at the begining. Harsh, unforviging, and so much fun. Existing on the planet called Norrath, created by the great crystaline Dragon (Veeshan, ruler of the Plane of Sky), lay claim to the planet by swiping her mightly claw and depositing her brood on the planet. the gods began to take notice of the planet and one by one they created their own races and set them down all across Norrath with different goals, aspirations and priorities in mind. You can read more about the lore by playing the game, or by checking out the 'Lore' folder. Know this ... racism is alive and well in Norrath, sadly the histories of the races extends from the gods themselves who have, for milleina, fought against each other for one reason or another. This has been ingrained into the races at a cosmic level and will be tough to overcome. Few have been successful in this feat, yet they have changed the world. Think carefully about the race you choose, it will change the difficulty of the game dramatically, if not completely obvious at first.")

    print("NeverText Lore: ")
    input("Press Enter")
    starthelp1()



def racelore():
    print("In EverQuest, there were originally 12 races and 14 classes, this was extended to higher numbers as expansions went on, but I am focusing on the originals for this game currently. Within NeverText, I have a small fraction built out. I will end up putting in all of the originals, it will take time.")
    input("Let's go through the races one by one, I will cover playable races and some non-playable, but important races -- Press Enter")
    osdetect.clear
    option = input("1. Dragons\n2. Dwarves")

    if option == "1":
        print("Dragons:\nThe oldest of all the races on Norrath, the dragons were here first. Veeshan deposited her brood here when she found Norrath millenia ago. They have high natural resistance, high natural wisdom and are known to typically be very social creatures. It is rare to find one living on their own, and if you ever encounter one *HINT HINT* It would be save to assume they are not only very powerful, but also have betrayed their own kind and have little to lose, and everything to gain.")
        
        print("Dragons are magical creatures, and they are dwingling in number. They have a troubled past from being hunted by other races, and each other. Many factions have formed over the years. Examples include: The followers of the Sleeper (Kerafyrm), Veeshan's loyal, and Trakanon's Brood (Though Trakanon technically is still loyal to Veeshan, he just had other priorities for a time), and the Ring of Scale. Veeshan's followers are those who are truly loyal to their mother and live to serve her. However, no being is without flaw. Some dragons are completely imcomatible at a biological level, even if they stem from the same root.")
        
        print("Two dragons of opposite elemental powers confronted the council with their desire to mate. Kildrukaun immediately dismissed any blessing and ordered the dragons from ever fulfilling their desires. These two were young still and very headstrong, regarding the council and its rules to be outdated when considering the growing influence and power of the second generation. They fled to unknown corners of Norrath and there, they bound their souls in a sacred union that would forge the first prismatic dragon. When knowledge of this event came to the council, Kildrukaun, a being of great wisdom and calm heart for regard of his own people looked upon the prismatic's creation as that of Veeshan's will, for conceivably unto him, only Veeshan herself could defy her great priest's rule. The council was in agreement and the hatchling, who would be named Kerafyrm, the Prophetic Savior, was allowed to continue existence under close watch that would be governed by Kildrukaun himself.")
        
        print("Although Kildrukaun was inspired and intrigued by Kerafyrm's existence, he was wary still for he had yet to be witness to the true intent of Veeshan's will. In this time, the dragon nation had split and were in opposing power -- the Ring of Scale whom fled their native Velious for Kunark, and the Claws of Veeshan, the original order and council of dragon kind. It was known that Kildrukaun was furious with the recent division and he strongly believed that it was Veeshan's will for the dragon nations to reunite once again, lest they become as weak and pitiful as the second generation. He viewed Kerafyrm to be the road that would guide the dragons to unification, although it was a unification that would be borne of an unknown purpose that Kerafyrm served. Kildrukaun never interacted with the young prismatic dragon, but served as the dictator behind his mentors. Eventually, Kerafyrm would rise to a power unseen within any dragon of his still fledgling age and Kildrukaun's prophecy would be born.")

        print("Through the course of many actions against the advisement of the counsel, Kerafyrm truly belived he could bring every dragon under one ruler -- him. Eventually he grew too powerful and a decision was made -- Kerafyrm had gained a significant following, further dividing the dragons. ")
        
        print("His fanatics were driven away and in a great struggle the first brood subdued him into a great sleep and sealed him within Sleeper's Tomb. Four dragons were left to perpetually guard the seal. Sadly, as ages have passed the first brood are just a handful. Some dragons have come to question the wisdom of sealing him away .. One of the largest threats to not only Norrath, but all the planes of existence could return.")
        input("Press Enter to return and select another race")
    elif option == "2":
        print("Dwarves:\nThe Dwarves are, as per the ancient texts, the second oldest race in Norrath. Created by Brell Serilis in the depths of Underfoot, that is the world below Norrath close to the core including a separate plane of existance called Underfoot, the Dwarves have a long history, and a violent one.")
        
        print("The First Dwarven-Ogre War")

        print("Eventually finding a home in the Jagged cliffs of what is now known as Butcherblock Mountains, the Dwarves founded Kaladim. Forming a strong alliance with the Ancient Elven empire, the Dwarves were aiding the Elves against the elven rebel armies. As word got out about this civil war, and the Dwarves aiding the old Kingdom, the Ogre's saw an opportunity. Up until this point, the Mountains were home to both the Ogres and the Dwarves. The Ogre's current leader, Dagnor the Butcher, was plotting against the Dwarves to take their home. ")


def classlore():
    print("In EverQuest, there were originally 12 races and 14 classes, this was extended to higher numbers as expansions went on, but I am focusing on the originals for this game currently. Within NeverText, I have a small fraction built out. I will end up putting in all of the originals, it will take time.")

def devprogress():
    pass