import sqlite3
import os
import sys


#THIS HAS TO BE DONE IN DBEAVER IN ORDER TO AUTO INCRIMENT. script saved in the reference scripts folder
#sqlite limitation with python i guess?
def createtable():
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    conn.execute("""CREATE TABLE IF NOT EXISTS PLAYER (
        id INT AUTOINCRIMENT PRIMARY KEY,
        Name TEXT,
        level INT,
        pclass INT,
        race INT,
        maxhealth INT,
        health INT,
        maxmana INT,
        mana INT,
        potions INT,
        exp INT,
        levelupxp INT,
        str INT,
        sta INT,
        agi INT,
        dex INT,
        cha INT,
        int INT,
        wis INT,
        gold INT,
        resist INT,
        quest TEXT,
        inventory TEXT,
        chest INT,
        arms INT,
        wrist INT,
        hands INT,
        legs INT,
        feet INT,
        head INT,
        neck INT,
        finger1 INT,
        finger2 INT,
        finger3 INT,
        finger4 INT,
        earleft INT,
        earright INT,
        primaryw INT,
        secondary INT,
        range INT,
        zone_id INT,
        melee INT,
        spells INT,
        diety TEXT,
        dietylore TEXT,
        ac INT,
        meleeskill INT,
        spellskill INT
        );""")
        
   
    conn.commit()
    conn.close()

def deleterow():
    conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
    conn.execute("DELETE FROM PLAYER WHERE name like lower('guin%')")
    conn.commit()
    conn.close()

deleterow()