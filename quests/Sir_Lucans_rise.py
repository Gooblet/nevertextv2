###################################################################################
#                   Sir Lucan D'lere's Rise
###################################################################################
'''
TODO:
Set zone ID 
set enemy range (this is going to require a complete rework of the enemy table)
set variables like # enemies to kill for successfull quest
change the start1 screen to not have an active shop because of the raiding undead
other logical changes
then finish this quest script
'''
import os
import sys
import time
import sqlite3

conn = sqlite3.connect('/Users/goober/Documents/Python/Py_NeverText/nevertextdatabase/nevertext.db')
c = conn.cursor()

for row in c.execute(f"SELECT * FROM NPC WHERE Name like '%Lucan%'"):
    queststarter = row[1]
    gm = row[4]


'''
Premise:
    Freeport has been gaining power and control since they have established themselves as the main trade route between Faydwer and Antonica. They are looking to tap into the potential of the Ocean of Tears. Unfortunately the Ocean of Tears is like the wilds of Kunark. Multiple factions establishing dominance within their plot of land, no one powerful enough to take complete control. 

    The Knights of Truth, holy Paladins of Freeport have built a relationship with the Sisters of Elrossi. They are planning a campaign to attempt to take control of some of the islands of OoT and expand their control, using the Sister's as a medium. The knights left the city of Freeport in the capable hands of Lucan D'lere, one of the younger Paladins, but one of the most capable. He commands a small group of men to keep Freeport safe. The Knights sailed off a week prior to your arrival...
'''
#Note -- need to put the quests into a function that I Call
def sirlucan():
    print(f"You enter the City of Freeport and make your way to the {row[4]} Guild to receive further training")
    print(f"Once you arrive you're greeted by {queststarter}")
    print(f"'Hail, Knight. How may I assist you?' {queststarter} asks.")
    print("'Sir Lucan, I heard the Knights of Truth were shorthanded due to a recent expedition to the Ocean of Tears and have come to offer my services.' You reply.")
    print("Ah, yes, well while I do appreciate the offer, I don't know what we could do with someone of your current experience. I need knights who can fight and defend. You look like you may be capable, but I also can't afford to babysit at the moment. When the Knights return from their quest come see me and we may discuss this further.")
    print("Disappointed, you turn and walk away. \n\n You rent a room at Hogcaller's inn. Because you traveled so far, you decided to stay and see what Freeport has to offer. Who knows, you may get your chance to serve the Knights.")
    input("Press Enter to sleep")
    os.system('clear')
    print("As night drew, and torches were fading to a dull red over the city, you fell asleep.\nYou awoke to the sound of screams and swords clashing together! You quickly dress yourself in your armor, draw your weapon and rush to face whatever dangers present themselves...")
    print("You get to the lobby of the Hogcaller's in and see civilians huddled at the back wall to your left, and some of the larger men barracading the front door to your right. You glance out the window and see Skeletons and other undead rampaging the city!\nYou jump out the nearest window and quickly take down 2 skeletons attempting to push open the front door.")
    print("'I will hold them out here, get everyone upstairs to a safe place, I will try to send anyone inside I can while still protecting you!' - You shout.\n Unfortunately you don't think you will be going anywhere for a while, you just attracked the attention of some of the undead terrorizing the nearby shops. They approach with a lust for death in their glowing green eyes...")

