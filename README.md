NeverText -- a python game from an old batch file i found on my computer

At this point in time, the game uses all default libraries of Python3

How to play:

In order to play, you need to build a sqlite database.

I am currently working on scripts to make this easier 

You need to make the following tables:
PLAYER
SPELL
MELEE
ENEMY
PCLASS
RACE
ITEMS

Most of the column names should be in the dbscripts folder in the respective files.
There are some sql queries i used to build the tables. you should be able to make the
tables with those and start playing. 

I will update this once the new dbcreation script is finished. 

There is currently an issue i'm working through with sqlite3 and python where the autoincriment flag
is not being set properly. i don't know if this is a limitaiton of the python package or if i am doing somethig wrong
Might have to update to a new database type in the future. 
If nothing else, i should be able to make a script that runs the queries and returns a success
