import os
import sys
import time

enemy = "Kobold Grunt"
elvl = 1
eatk = 4
ehp = 35
ehpm = 35
player = "Maltris"
lvl = 4
pclass = "cleric"
pattack = 3
asn = "Strike"
asd = 6
asc = 9
hsn = "Light Healing"
hsc = 7
hsa = 12
hp = 30
hpmax = 30
mana = 45
manamax = 45
    #help()

def tut_help():
    os.system('clear')
    print("You awaken suddenly to find yourself being pulled along in a cart on a dusty dirt road.")
    print("""Your hands stained with blood and your head aching immensely.\n You go to move your hands to your head to attempt to assess the damage and a chain stops your hands from rising.\n You are chained to the floor of the cart. Guteral noises can be heard outside of the cart. You notice other bodies near you on the cart.\n Most appear to be dead. How did you get here?
    """)
    input("Press Enter to continue")
    os.system('clear')
    print("""Suddenly you hear the sound of steel clashing together. Gutteral noises and what sounds like humans shouting -- a fight!. Is someone here to save you?

    You assess the integrity of the chains binding you to the floor. They feel sturdy enough and you are too lightheaded to stand and pull with any considerable effort. 

    You sit back down and try to see outside through the cracks in the walls of the wooden cart. The cart has stopped. 

    The noise dies down, you hear low voices talking but cannot make out anything specific.\n Suddenly, the door opens.\n You see a massive figure appear in the light of the outside world.\n He walks into the cart without saying a word and assesses the fallen corpses on the ground.

    "This one is still alive!" he shouts after checking the pulse of a small elf on the ground. \nWood elf, you assessed based on the thin, small stature and golden brown hair, \ntrademark of the wood elves of kelethin. 

    Where have you been that you would be captured with a wood elf. You have never been to the continent of Faydwer personally. 

    The large figure checks a few more bodies, notes them deceased and moves over to you. He takes his sword and breaks the chains. 

    "Can you stand?" he asks. 

    You nod wearily, still not completely sure if this man is your ally or your enemy. \nHe helps you to your feet and escorts you out of the cart. \nYou are placed in the back of a wagon now with a high elven woman. \nSilently she begins to assess your damages. She casts a spell on you and you instantly feel better. \nA cleric, you surmise. 

    "How does that feel?" She asks with a flat tone, almost annoyed to have to be dealing with you. 

    "Better." You respond quickly as to not irrirate your savior further. 
    """)
    input("> press enter to continue")
    os.system('clear')
    print("""

    She moves onto others as they are brought to the wagon. \n"This one might not make it in her current state." she says, referring to the frail, bloodied half elf. \n"We need more powerful healing than I can provide." \nThe breathing of the half elf begins to become shallow and rapid. \n"May the goddess Tunare guide you on your new journey Half Elf."

    A wave of memories spur back into your head all at once. \nYou remember you are a Cleric yourself! \nYou immediately rush over to the half elf and begin casting your most powerful spell your weakened body can muster. \n"It's too late Human, we don't have the power to save her." \nYou ignore her and push harder and out explodes white light from your palms covering the half elf. \nYou feel something foul. Poison! \nIt's faint which means it has moved deep inside the Half Elf but you can detect it. \nThe High Elf Cleric must not have been looking for poison. 

    "She has been poisoned. My healing magic won't last long with that poison in her." You say quickly. 

    "I don't sense anythi -- wait, there it is, it's faint. How did you find that? It's far in there." \nThe High Elf begins to cast a spell of her own. \nA faint blue light emmits from her hands and she presses her hands against the little elf. \nYou are still pushing your magic out with every ounce of strength you have left in you to sustain the elf while the poison is removed. 
    
    """)
    input("Press Enter")
    os.system('clear')
    print("""
    Eventually the poison is clear, the elf is allowed time to rest and the High Elf thanks you for your services. \nShe explains they are part of a rebellion against the Kobolds of GloomingDeep. \nA few years ago the Kobolds got a new leader after a coup within the Kobold conglomerate and this new leader is power hungry, and wants to build an empire. \nHe has been sending out soldiers to kidnap anyone they could to use as slave labor. 

    The first months in the caves were the worst as the new leader was removing the old hirearchy if they spoke out against him in any way. \nThose he replaced, were replaced with insane Kobolds who would beat the slaves for no reason other than boredom. \nThen the rebellion formed. \nArias, a Barbarrian Warrior led the rebellion against the slavers and most of us were able to escape. 

    "We took over one of the miners camps, setup guards and have been at war with the Kobolds while some of the less combat skilled miners work on digging a tunnel out. "

    "We finally broke through, but a decision was made to stay and finish off the Kobolds otherwise they would more than likely, try to rebuild. \nWe setup and trained scouts and as Kobolds left the caverns we sent a group to follow them and save whomever they kidnapped. \nWe have been fairly successful, until now. \nThey never killed the new slaves before. \nYou were lucku to be alive. " \nThe High elf finished her story as a cry was heard from ahead the road.

    "AMBUSH!" one of the scouts shouted. 

    "Can you fight?" The Cleric asks.

    You nod, eagerly. 
    """)
    input("> Press ENTER to continue")
    '''
    global enemy
    global elvl
    global eatk
    global ehp
    global ehpm
    global player
    global lvl
    global pclass
    global pattack
    global asn
    global asd
    global asc
    global hsn
    global hsc
    global hsa
    global hp
    global hpmax
    global mana
    global manamax

    enemy = "Kobold Grunt"
    elvl = 1
    eatk = 4
    ehp = 35
    ehpm = 35
    player = "Maltris"
    lvl = 4
    pclass = "cleric"
    pattack = 3
    asn = "Strike"
    asd = 6
    asc = 9
    hsn = "Light Healing"
    hsc = 7
    hsa = 10
    hp = 30
    hpmax = 30
    mana = 45
    manamax = 45
    '''
    prefight1()

def prefight1():
    os.system('clear')
    print("You are about to fight your first enemy. \nWe will walk through the basics of combat, some simple strategies and what you should be prepared for.")
    input("Press Enter to continue")
    os.system('clear')
    print(f"You have been attacked by a {enemy}!")
    input("Press Enter to continue")
    os.system('clear')
    print(f"{enemy} - LeveL: {elvl} - HP: {ehp} / {ehpm}")
    print(f"\n\nLooks like you have been attacked by a {enemy}. \nThis Kobold is one of the more basic soldiers we encounter. They have a good amount of hp and can hit for a decent amount of damage. \nGood thing you're a cleric.\n")
    print("Now let's take a look at your stats.")
    print(f"{player}")
    print(f"Level:{lvl} - {pclass}:")
    print(f"{hp} / {hpmax} -- {mana} / {manamax}")
    print("\nAbove you can see an example of what stats you might see in the game. Now let's try attacking the Grunt.")
    input("Press Enter to continue")
    fight1()

def fight1(): #lots of bugs with formatting here
    global hp
    global ehp
    global mana
    global hpmax
    global manamax
    healthDashes = 10
    os.system('clear')
    print(f"{player}")
    print(f"Level:{lvl} - {pclass}:")
    print(f"{hp} / {hpmax} -- {mana} / {manamax}")
    dashConvert = int(hpmax/healthDashes)                         
    currentDashes = int(hp/dashConvert)                           
    remainingHealth = healthDashes - currentDashes                    

    healthDisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingHealth)]) 
    percent = str(int((hp/hpmax)*100)) + "%"                 

    print("|" + healthDisplay + remainingDisplay + "|")               
    print("     " + percent) 
    print("--------------------------------------------")
    print(f"{enemy} - LeveL: {elvl} - HP: {ehp} / {ehpm}")
    edashConvert = int(ehpm/healthDashes)                         
    ecurrentDashes = int(ehp/edashConvert)                           
    eremainingHealth = healthDashes - ecurrentDashes                    

    ehealthDisplay = ''.join(['█' for i in range(ecurrentDashes)])      
    eremainingDisplay = ''.join([' ' for i in range(eremainingHealth)]) 
    percent = str(int((ehp/ehpm)*100)) + "%"                 

    print("|" + ehealthDisplay + eremainingDisplay + "|")               
    print("     " + percent) 
    print("What do you want to do?")
    option = input("1. Melee Attack\n2. Heal\n3. Cast Strike\n")
    
    if option == "1":
        ehp -= pattack
        print(f"Good! You struck the enemy for {pattack} points of damage.")
        print("Though that's not a lot. Let's see what the enemy does. Brace yourself!")
        input("Press Enter.")
        if ehp <= 0:
            win()
        print(f"The {enemy} crushes you for {eatk} points of damage!")
        hp -= eatk
        print("Ouch. Don't worry, you're fine.")
        input("Let's take a look at the stats now. Press Enter")
        if hp <= 0:
            die()
        else:
            fight1()
    elif option == "2":
        if mana <= hsc:
            input("You do not have enough mana to cast this spell\nPress Enter")
        elif hp == hpmax:
            print("You are at max health, you don't want to waste the mana casting a spell that won't do anything!")
            input("Press Enter to select another option")
            fight1()
        else:
            input("You have healed yourself!\nPress Enter")
            hp += hsa
            mana -= hsc
            print("Now the enemy will attack, prepare yourself!")
            input("Press Enter.")
            print(f"The {enemy} crushes you for {eatk} points of damage!")
            hp -= eatk
            print("Ouch. Don't worry, you're fine.")
            input("Let's take a look at the stats now. Press Enter")
            if hp >= hpmax:
                hp = hpmax
                fight1()
            else:
                fight1()
    elif option == "3":
        if mana <= asc:
            input("You do not have enough mana to cast this spell\nPress Enter")
            fight1()
        else:
            print(f"You begin to cast the spell {asn}...")
            time.sleep(2)
            print(f"You do {asd} damage to {enemy}")
            ehp -= asd
            mana -= asc
            print("Well done! Sometimes spells do more damage than a melee attack")
            input("Keep an eye on your mana. Spells cost mana and if you run out, you can't cast spells.\npress Enter to continue.")
            if ehp <= 0:
                win()
            else:
                print("Now the enemy will attack, prepare yourself!")
                input("Press Enter.")
                print(f"The {enemy} crushes you for {eatk} points of damage!")
                hp -= eatk
                print("Ouch. Don't worry, you're fine.")
                input("Let's take a look at the stats now. Press Enter")
                fight1()
    else:
        input("Make sure you select an available option from the menu.\nPress Enter to try again")
        fight1()

def die():
    print("You died. Maybe try another tactic to beat the Kobold?")
    input("Press Enter to be returned to the fight to try again.")
    fight1()


def win():
    os.system('clear')
    print("The Kobold falls to the ground. You slowly approach it.")
    print("You kick the Kobold, it doesn't move.")
    print("The rest of the rebellion approaches you as they take down their foes")
    print("A large man walks up to you.")
    print("'I am Arias, you have some skills. We could use talent like yours.")
    print("I saw what you did for the Wood Elf. Your healing skills are impressive.")
    print("We're about done here. Heading back to the camp.")
    print("Would you come with us and help our cause?")
    print("The Kobolds won't quit until they are completely defeated.")
    print("We're low on healing magic, your skills would greately improve our chances of victory'")
    print("He looks at you hopefully.")
    print("Do you want to join the rebellion?")
    option = input("1. Yes\n2. No\n")
    if option == "1":
        input("Welcome to the rebellion, Maltris.\nPress Enter to fight the good fight")
        os.system('python3 -m nevertext.py')
    elif option == "2":
        exit
    else:
        win()
