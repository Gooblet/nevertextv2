import sqlite3

conn = sqlite3.connect('nevertext.db')

######
#Create table
######
def createenemytable():
    conn.execute(f"""CREATE TABLE IF NOT EXISTS ENEMY (
        id INTEGER PRIMARY KEY,
        Name TEXT,
        maxhealth INT,
        health INT,
        maxmana INT,
        mana INT,
        goldgain INT,
        str INT,
        sta INT,
        agi INT,
        dex INT,
        cha INT,
        int INT,
        wis INT,
        resist INT,
        itemdrop INT,
        melee INT,
        spells INT,
        expgain INT);""")

#Add Enemy to table
#Level 1 skeleton
def enemycreate():
    conn.execute(f"""INSERT INTO ENEMY ( id, Name, maxhealth, health, maxmana, mana, goldgain, str, sta, agi, dex, cha, int, wis, resist, itemdrop, melee, spells, expgain) VALUES (2, 'A Decaying Skeleton', 25, 25, 10, 10, 5, 5, 6, 5, 8, 1, 3, 3, 0, 0, 1, 1, 100);""")
    conn.commit()

    
def deleterow():
    conn.execute("DELETE FROM PLAYER WHERE Name like lower('%')")
    conn.commit()

enemycreate()