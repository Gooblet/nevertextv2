#move to topmenu() with proper variable assignments
#This is also used to test the progress bar i want to include for experience
health = 80        
maxHealth = 200    
healthDashes = 20  

expmax = 100
exp = 40
expdashes = 20

distancemax = 500
distance = 170
distancedashes = 100

def do_health():
    dashConvert = int(maxHealth/healthDashes)                         
    currentDashes = int(health/dashConvert)                           
    remainingHealth = healthDashes - currentDashes                    

    healthDisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingHealth)]) 
    percent = str(int((health/maxHealth)*100)) + "%"                 

    print("|" + healthDisplay + remainingDisplay + "|")               
    print("         " + percent)                                      


def expbar():
    dashConvert = int(expmax/expdashes)                         
    currentDashes = int(exp/dashConvert)                           
    remainingexp = expdashes - currentDashes                    

    expdisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingexp)]) 
    percent = str(int((exp/expmax)*100)) + "%"                 

    print("      " + "EXP to level")
    print("|" + expdisplay + remainingDisplay + "|")               
    print("         " + percent)   

def distancebar():
    dashConvert = int(distancemax/distancedashes)                         
    currentDashes = int(distance/dashConvert)                           
    remainingdistance = distancedashes - currentDashes                    

    distdisplay = ''.join(['█' for i in range(currentDashes)])      
    remainingDisplay = ''.join([' ' for i in range(remainingdistance)]) 
    percent = str(int((distance/distancemax)*100)) + "%"                 

    print("      " + "distance traveled")
    print("|" + distdisplay + remainingDisplay + "|")               
    print("         " + percent)  


do_health()