CREATE TABLE SPELL (
id integer primary key AUTOINCREMENT,
Name TEXT,
level integer,
pclass integer,
damage integer, 
heal integer,
manacost integer,
melee integer,
spell integer);