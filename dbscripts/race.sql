CREATE TABLE RACE (
id integer primary key AUTOINCREMENT,
racename TEXT,
str integer,
sta integer,
agi integer, 
dex integer,
cha integer,
int integer,
wis integer,
resist integer,
diety TEXT,
dietylore TEXT
);