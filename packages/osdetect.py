###################################################################################
#                   OS Detect
#                   Verision 1.0
#                   Focus: Check os and assign relevant variables to commands 
#                   Creator: Nic Weilbacher
###################################################################################

import os
import sys


def osdetect():
    """
    Operating system detection function

    This function detects the operating system running on the computer to execute the correct terminal commands

    """
    
    global tclear
    platform = sys.platform
    tclear = ''
    platform
    if platform == 'darwin':
        tclear = 'clear'
    elif platform == 'linux' or platform == 'linux32':
        tclear = 'clear'
    elif platform == 'win32':
        tclear = 'cls'
    else:
        print("Cannot determine what operating system you're running on. \nThis Application can run on linux, windows, and mac only")
        print("If you're running on a toaster,  please update to toaster2.0 running debian to play")
        input("press enter to exit the program and fix the error")
        sys.exit()
